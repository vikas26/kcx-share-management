<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/share', 'ShareController@index')->name('share');

Route::get('/share/add', 'ShareController@getAddShare')->name('getAddShare');
Route::post('/share/add', 'ShareController@postAddShare')->name('postAddShare');

Route::get('/share/edit/{id}', 'ShareController@getEditShare')->name('getEditShare');
Route::post('/share/edit', 'ShareController@postEditShare')->name('postEditShare');

Route::get('/share/delete/{id}', 'ShareController@getDeleteShare')->name('getDeleteShare');
