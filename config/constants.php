<?php

return [
    'SHARE_INSTRUMENT_NAMES' => array('A', 'B', 'Preferred A', 'Preferred B'),
    'DEFAULT_NUMBER_OF_ITEMS' => 5,
    'STATIC_FILE_VERSION' => 1.1,
    'TRANSACTION_VIEW_TZ' => 'America/New_York'
];
