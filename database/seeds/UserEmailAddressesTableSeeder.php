<?php

use Illuminate\Database\Seeder;

class UserEmailAddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_email_addresses')->insert([[
            'user_id' => 1,
            'email_address' => 'vikas@gmail.com',
            'is_default' => '0',
            'created_at' => date("Y-m-d H:i:s"),
        ],
        [
            'user_id' => 1,
            'email_address' => 'vikas26294@gmail.com',
            'is_default' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        ],
        [
            'user_id' => 2,
            'email_address' => 'vikas26@gmail.com',
            'is_default' => '1',
            'created_at' => date("Y-m-d H:i:s"),
        ]]);
    }
}
