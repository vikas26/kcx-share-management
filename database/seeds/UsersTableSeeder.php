<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([[
            'id' => 1,
            'first_name' => 'Vikas',
            'last_name' => '',
            'password' => bcrypt('vikas26'),
            'created_at' => date("Y-m-d H:i:s"),
        ],
        [
            'id' => 2,
            'first_name' => 'Vikas',
            'last_name' => 'Balwada',
            'password' => bcrypt('vikas26'),
            'created_at' => date("Y-m-d H:i:s"),
        ]]);
    }
}
