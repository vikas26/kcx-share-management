<?php

use Faker\Generator as Faker;

$factory->define(App\UserEmailAddress::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
        'email_address' => $faker->unique()->safeEmail,
        'is_default' => '1'
    ];
});
