<?php

use Faker\Generator as Faker;

$factory->define(App\SharePurchase::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(App\UserEmailAddress::class)->create()->user_id;
        },
        'company_name' => $faker->name,
        'share_instrument_name' => $faker->randomElement($array = config('constants.SHARE_INSTRUMENT_NAMES')),
        'quantity' => $faker->numberBetween($min = 1, $max = 100),
        'price' => $faker->randomFloat($nbMaxDecimals = 10, $min = 10, $max = 10000) ,
        'total_investment' => $faker->numberBetween($min = 100, $max = 10000),
        'certificate_number' => $faker->name,
    ];
});
