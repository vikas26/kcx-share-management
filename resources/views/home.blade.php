@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="">
                        <a class="btn btn-primary" href="{{ route('getAddShare') }}">Add Shares</a>
                    </div>

                    <div class="">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('share.index')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
