@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <a class="btn btn-primary" href="{{ route('getAddShare') }}">Purchase Share</a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Purchased shares</div>

        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('status') }}
                </div>
            @endif

            <div class="section">
                @if (count($added_shares))
                    <table class="table table-striped">
                        <thead>
                            <th>S.No</th>
                            <th>Company</th>
                            <th>Bought on <br>(in {{ config('constants.TRANSACTION_VIEW_TZ') }} TZ)</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                            @foreach ($added_shares as $key => $one_added_share)
                                <tr>
                                    <td>{{ $key + 1 + $offset }}</td>
                                    <td>{{ $one_added_share->company_name }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $one_added_share->created_at)->timezone(config('constants.TRANSACTION_VIEW_TZ'))->format('Y-m-d') }}</td>
                                    <td>
                                        <a href="{{ route('getEditShare', $one_added_share->id) }}" class="btn btn-info">Edit</a>
                                        <button onClick="confirmAndDeleteRecord(this)" data-href="{{ route('getDeleteShare', $one_added_share->id) }}" class="btn btn-danger">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    No records found
                @endif
                
                {{ $added_shares->links() }}

            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">

    // confirm and delete record
    function confirmAndDeleteRecord(this_el)
    {
        if (confirm("Are you sure you want to delete this record?") == true)
        {
            window.location.href = this_el.dataset['href'];
        }
    }
</script>
@endsection