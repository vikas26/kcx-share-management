@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <a class="btn btn-primary" href="{{ route('share') }}">Home</a>
            <a class="btn btn-primary" href="{{ route('getAddShare') }}">Purchase Share</a>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Purchased Shares
        </div>

        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('status') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('error') }}
                </div>
            @endif

            <form class="form-horizontal" method="POST" action="{{ route('postEditShare') }}">
                {{ csrf_field() }}
                
                <input type="number" name="id" value="{{ $added_share->id}}" hidden/>

                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                    <label for="company_name" class="col-md-4 control-label">Company*</label>

                    <div class="col-md-6">
                        <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name', $added_share->company_name) }}" required autofocus>

                        @if ($errors->has('company_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('company_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('share_instrument_name') ? ' has-error' : '' }}">
                    <label for="share_instrument_name" class="col-md-4 control-label">Share Instrument Name*</label>

                    <div class="col-md-6">
                        <select id="share_instrument_name" name="share_instrument_name" required>
                            <option>Please choose</option>
                            @foreach ($share_instrument_names as $share_instrument_name)
                                <option value="{{ $share_instrument_name }}" {{ $share_instrument_name == old('share_instrument_name', $added_share->share_instrument_name) ? 'selected' : '' }}>{{ $share_instrument_name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('share_instrument_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('share_instrument_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                    <label for="quantity" class="col-md-4 control-label">Quantity*</label>

                    <div class="col-md-6">
                        <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity', $added_share->quantity) }}" required min="1" step="1">

                        @if ($errors->has('quantity'))
                            <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label for="price" class="col-md-4 control-label">Price*</label>

                    <div class="col-md-6">
                        <input id="price" type="number" class="form-control" name="price" value="{{ old('price', $added_share->price) }}" required min="1" step="any">

                        @if ($errors->has('price'))
                            <span class="help-block">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('total_investment') ? ' has-error' : '' }}">
                    <label for="total_investment" class="col-md-4 control-label">Total Investment*</label>

                    <div class="col-md-6">
                        <input id="total_investment" type="number" class="form-control" name="total_investment" value="{{ old('total_investment', $added_share->total_investment) }}" required min="1" step="1">

                        @if ($errors->has('total_investment'))
                            <span class="help-block">
                                <strong>{{ $errors->first('total_investment') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('certificate_number') ? ' has-error' : '' }}">
                    <label for="certificate_number" class="col-md-4 control-label">Certificate number*</label>

                    <div class="col-md-6">
                        <input id="certificate_number" type="text" class="form-control" name="certificate_number" value="{{ old('certificate_number', $added_share->certificate_number) }}" required>

                        @if ($errors->has('certificate_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('certificate_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
