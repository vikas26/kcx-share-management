<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the email addresses for a user.
     */
    public function userEmailAddresses()
    {
        return $this->hasMany('App\UserEmailAddress');
    }

    /**
     * Get user by Email address
     *
     * @return Object
     */
    public static function getUserByEmail($email)
    {
        return self::join('user_email_addresses', 'user_email_addresses.user_id', '=', 'users.id')
                ->where('user_email_addresses.email_address', $email)
                ->select(array('users.id', 'users.first_name', 'users.password', 'users.remember_token', 'user_email_addresses.email_address', 'user_email_addresses.is_default'))
                ->get()
                ->first();
    }

    /**
     * Create a new user resource
     *
     * @return Object
     */
    public static function createNewResource($user_data)
    {
        return self::create($user_data);
    }
}
