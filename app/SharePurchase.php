<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SharePurchase extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'share_purchases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_name',
        'share_instrument_name',
        'price',
        'quantity',
        'total_investment',
        'certificate_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Get user added shares
     *
     * @return Object
     */
    public static function getUserShares($user_id, $items_count)
    {
        return self::where('user_id', '=', $user_id)
                ->orderBy('created_at', 'desc')
                ->paginate($items_count);
    }

    /**
     * Get particular added share of a user
     *
     * @return Object
     */
    public static function getParticularShareOfUser($id, $user_id)
    {
        return self::where('id', '=', $id)
                ->where('user_id', '=', $user_id)
                ->get()
                ->first();
    }

    /**
     * Create new resource
     *
     * @return Object
     */
    public static function createNewResource($data)
    {
        return self::create($data);
    }

    /**
     * Delete resource
     *
     * @return Object
     */
    public static function deleteResource($id, $user_id)
    {
        return self::where('id', '=', $id)
                ->where('user_id', '=', $user_id)
                ->delete();
    }

    /**
     * Update resource
     *
     * @return Object
     */
    public static function updateResource($id, $user_id, $data)
    {
        return self::where('id', '=', $id)
                ->where('user_id', '=', $user_id)
                ->update($data);
    }

}
