<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AddShare extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'          => 'required',
            'share_instrument_name' => array('required', Rule::in(config('constants.SHARE_INSTRUMENT_NAMES'))),
            'price'                 => 'required|numeric',
            'quantity'              => 'required|integer|min:1',
            'total_investment'      => 'required|integer',
            'certificate_number'    => 'required',
        ];
    }
}
