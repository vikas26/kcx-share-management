<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\AddShare;
use App\Http\Requests\EditShare;

use App\SharePurchase;

class ShareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the shares
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('share.index', array(
            'user' => Auth::user(),
            'added_shares' => SharePurchase::getUserShares(Auth::id(), config('constants.DEFAULT_NUMBER_OF_ITEMS')),
            'offset' => ($request->input('page', 1) - 1) * config('constants.DEFAULT_NUMBER_OF_ITEMS')
        ));
    }

    /**
     * Blank add share view
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddShare()
    {
        return view('share.add', array('share_instrument_names' => config('constants.SHARE_INSTRUMENT_NAMES')));
    }

    /**
     * Add new share
     *
     * @return \Illuminate\Http\Response
     */
    public function postAddShare(AddShare $request)
    {
        // collect post params and create new resource
        $is_successful = SharePurchase::createNewResource(array(
            'user_id'               => Auth::id(),
            'company_name'          => $request->post('company_name'),
            'share_instrument_name' => $request->post('share_instrument_name'),
            'price'                 => $request->post('price'),
            'quantity'              => $request->post('quantity'),
            'total_investment'      => $request->post('total_investment'),
            'certificate_number'    => $request->post('certificate_number'),
        ));

        // error while saving data
        if (!$is_successful)
        {
            return redirect()->back()->withInput()->with('error', 'Internal server error.');
        }

        return redirect()->route('share')->with('status', 'Successfully added.');
    }

    /**
     * Delete share 
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeleteShare($id)
    {
        if (!SharePurchase::deleteResource($id, Auth::id()))
        {
            return redirect()->route('share')->with('error', 'Internal server error.');
        }
        return redirect()->route('share')->with('status', 'Successfully deleted.');
    }

    /**
     * Delete share 
     *
     * @return \Illuminate\Http\Response
     */
    public function getEditShare($id)
    {
        return view('share.edit', array(
            'share_instrument_names' => config('constants.SHARE_INSTRUMENT_NAMES'),
            'added_share' => SharePurchase::getParticularShareOfUser($id, Auth::id()),
        ));
    }

    /**
     * Update resource
     *
     * @return \Illuminate\Http\Response
     */
    public function postEditShare(EditShare $request)
    {
        // collect post params
        $id = $request->post('id');

        // create new resource
        $is_successful = SharePurchase::updateResource($id, Auth::id(), array(
            'company_name'          => $request->post('company_name'),
            'share_instrument_name' => $request->post('share_instrument_name'),
            'price'                 => $request->post('price'),
            'quantity'              => $request->post('quantity'),
            'total_investment'      => $request->post('total_investment'),
            'certificate_number'    => $request->post('certificate_number'),
        ));

        // error while saving data
        if (!$is_successful)
        {
            return redirect()->back()->withInput()->with('error', 'Internal server error.');
        }

        return redirect()->route('share')->with('status', 'Successfully updated.');
    }

}
