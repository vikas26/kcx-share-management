<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\UserEmailAddress;
use App\SharePurchase;

class SharePurchaseTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Add new share purchase without data
     *
     * @return void
     */
    public function testPostAddShare()
    {
        echo "-- Add new share --\n";

        // create new user
        $user = factory(User::class)->create();
        
        // create email
        factory(UserEmailAddress::class)->create([
            'user_id' => $user->id,
        ]);

        // randomly selected share instrument name
        $share_instrument_name = config('constants.SHARE_INSTRUMENT_NAMES')[array_rand(config('constants.SHARE_INSTRUMENT_NAMES'), 1)];

        // add new share - company_name is not provided
        $response = $this->actingAs($user)
                        ->json('POST', route('postAddShare'), array(
                            'share_instrument_name' => $share_instrument_name,
                            'price' => rand(10, 1000),
                            'quantity' => rand(10, 1000),
                            'total_investment' => rand(10, 1000),
                            'certificate_number' => 'A certificate',
                        ));

        // get response data
        $content = $response->getOriginalContent();

        // error response code
        $response->assertStatus(422);

        // company_name is found in errors
        $this->assertEquals(true, array_key_exists('company_name', $content['errors'])); 
    }

    /**
     * Add new share purchase with data
     *
     * @return void
     */
    public function testPostAddShareSuccess()
    {
        echo "-- Add new share success --\n";

        // create new user
        $user = factory(User::class)->create();
        
        // create email
        factory(UserEmailAddress::class)->create([
            'user_id' => $user->id,
        ]);

        // randomly selected share instrument name
        $share_instrument_name = config('constants.SHARE_INSTRUMENT_NAMES')[array_rand(config('constants.SHARE_INSTRUMENT_NAMES'), 1)];      

        // add new share - all the fields are provided
        $response = $this->actingAs($user)
                        ->json('POST', route('postAddShare'), array(
                            'company_name' => 'ABC-'.rand(10, 1000),
                            'share_instrument_name' => $share_instrument_name,
                            'price' => rand(10, 1000),
                            'quantity' => rand(10, 1000),
                            'total_investment' => rand(10, 1000),
                            'certificate_number' => 'A certificate',
                        ));

        // as it redirects user to share home page
        $response->assertStatus(302);
        $response->assertRedirect(route('share'));
        $response->assertSessionHas('status', 'Successfully added.');

        // follow to next page and this content should be there
        $this->followRedirects($response)->assertSee('Successfully added.');
    }

    /**
     * Edit share purchase without data
     *
     * @return void
     */
    public function testPostEditShare()
    {
        echo "-- Edit Share --\n";

        // create new user
        $user = factory(User::class)->create();
        
        // create email
        factory(UserEmailAddress::class)->create([
            'user_id' => $user->id,
        ]);

        // create share purchase
        $share_purchase = factory(SharePurchase::class)->create([
            'user_id' => $user->id,
        ]);

        // new quantity
        $new_quantity = $share_purchase->quantity + 10;

        // edit share - all the fields are provided
        $response = $this->actingAs($user)
                        ->json('POST', route('postEditShare'), array(
                            'id' => $share_purchase->id,
                            'share_instrument_name' => $share_purchase->share_instrument_name,
                            'price' => $share_purchase->price,
                            'quantity' => $new_quantity,
                            'total_investment' => $share_purchase->total_investment,
                            'certificate_number' => $share_purchase->total_investment,
                        ));

        // get response data
        $content = $response->getOriginalContent();

        // as it redirects user to share home page
        $response->assertStatus(422);

        // company_name is found in errors
        $this->assertEquals(true, array_key_exists('company_name', $content['errors'])); 

        // follow to next page and this content should be there
        $this->followRedirects($response)->assertSee('The company name field is required');
    }

    /**
     * Edit share purchase with data
     *
     * @return void
     */
    public function testPostEditShareSuccess()
    {
        echo "-- Edit Share Success --\n";

        // create new user
        $user = factory(User::class)->create();
        
        // create email
        factory(UserEmailAddress::class)->create([
            'user_id' => $user->id,
        ]);

        // create share purchase
        $share_purchase = factory(SharePurchase::class)->create([
            'user_id' => $user->id,
        ]);

        // new quantity
        $new_quantity = $share_purchase->quantity + 10;

        // edit share - all the fields are provided
        $response = $this->actingAs($user)
                        ->json('POST', route('postEditShare'), array(
                            'id' => $share_purchase->id,
                            'share_instrument_name' => $share_purchase->share_instrument_name,
                            'company_name' => $share_purchase->company_name,
                            'price' => $share_purchase->price,
                            'quantity' => $new_quantity,
                            'total_investment' => $share_purchase->total_investment,
                            'certificate_number' => $share_purchase->total_investment,
                        ));

        // as it redirects user to share home page
        $response->assertStatus(302);
        $response->assertRedirect(route('share'));
        $response->assertSessionHas('status', 'Successfully updated.');

        // follow to next page and this content should be there
        $this->followRedirects($response)->assertSee('Successfully updated.');
    }

    /**
     * Delete purchased share 
     *
     * @return void
     */
    public function testGetDeleteShare()
    {
        echo "-- Delete Share --\n";

        // create new user
        $user = factory(User::class)->create();
        
        // create email
        factory(UserEmailAddress::class)->create([
            'user_id' => $user->id,
        ]);

        // create share purchase
        $share_purchase = factory(SharePurchase::class)->create([
            'user_id' => $user->id,
        ]);

        // edit share - all the fields are provided
        $response = $this->actingAs($user)
                        ->json('GET', route('getDeleteShare', $share_purchase->id));

        // as it redirects user to share home page
        $response->assertStatus(302);
        $response->assertRedirect(route('share'));
        $response->assertSessionHas('status', 'Successfully deleted.');

        // follow to next page and this content should be there
        $this->followRedirects($response)->assertSee('Successfully deleted.');
    }
}
