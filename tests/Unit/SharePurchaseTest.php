<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\SharePurchase;

class SharePurchaseTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Get user share test
     *
     * @return void
     */
    public function testGetUserShare()
    {
        echo "-- Get share --\n";

        // create a new user with email and add one share purchase record
        $first_share_row = factory(SharePurchase::class)->create();

        // get share purchase records
        $user_shares = SharePurchase::getUserShares($first_share_row->user_id, 5);
        
        // as we have only inserted one row, it should be one
        $this->assertCount(1, $user_shares);

        // share instrument name should be one of them
        $this->assertContains($user_shares[0]->share_instrument_name, config('constants.SHARE_INSTRUMENT_NAMES'));
    }

    /**
     * Get particular share purchase of user
     *
     * @return void
     */
    public function testGetParticularShareOfUser()
    {
        echo "-- Get particular share --\n";

        // create a new user with email and add one share purchase record
        $first_share_row = factory(SharePurchase::class)->create();

        // second share purchased by different user
        $second_share_row = factory(SharePurchase::class)->create();

        // get particular share purchase of a user
        $user_share = SharePurchase::getParticularShareOfUser($first_share_row->id, $first_share_row->user_id);

        // will fetch user share row as this user has created this share
        $this->assertEquals($first_share_row->id, $user_share->id);

        // get particular share purchase of a user
        $user_share = SharePurchase::getParticularShareOfUser($first_share_row->id, $second_share_row->user_id);
        
        // will not fetch user share row as this user has not created this share
        $this->assertNull($user_share);
    }

    /**
     * Delete your share purchase
     *
     * @return void
     */
    public function testDeleteUserShare()
    {
        echo "-- Delete share --\n";

        // create a new user with email and add one share purchase record
        $first_share_row = factory(SharePurchase::class)->create();

        // try deleting resource of other users
        $is_deleted = SharePurchase::deleteResource($first_share_row->id, $first_share_row->user_id - 1);
        
        // this should not be deleted
        $this->assertEquals(false, $is_deleted);

        // delete created resource
        $is_deleted = SharePurchase::deleteResource($first_share_row->id, $first_share_row->user_id);
        
        // same user can delete resource
        $this->assertEquals(true, $is_deleted);
    }

    /**
     * Update your share purchase
     *
     * @return void
     */
    public function testUpdateUserShare()
    {
        echo "-- Updfate share --\n";

        // create a new user with email and add one share purchase record
        $first_share_row = factory(SharePurchase::class)->create();

        // second share purchased by different user
        $second_share_row = factory(SharePurchase::class)->create();

        // new quantity of a share
        $new_share_quantity = (int)$first_share_row->quantity + 1;

        // try updating resource of other users
        $is_updated = SharePurchase::updateResource($first_share_row->id, $second_share_row->user_id, array(
            'quantity' => $new_share_quantity
        ));

        // this should not be updated
        $this->assertEquals(false, $is_updated);

        // try updating resource of other users
        $is_updated = SharePurchase::updateResource($first_share_row->id, $first_share_row->user_id, array(
            'quantity' => $new_share_quantity
        ));

        // same user can update his/her created resource
        $this->assertEquals(true, $is_updated);

        // get particular share purchase of a user
        $user_share = SharePurchase::getParticularShareOfUser($first_share_row->id, $first_share_row->user_id);

        // is quantity updated
        $this->assertEquals($user_share->quantity, $new_share_quantity);
    }
}
