## Overview
Its a share purchase and management system with Authentication. 

## Stack used
- [Laravel 5.5](https://laravel.com/docs/5.5)
- PHP 7.0.26
- MySQL 5.7
- JQuery 3.2.1
- Bootstarp 3.3.7

## Highlights
- Unit and feature testing using PHPUnit
- Used DB migrations, seeds, factories
- Database access from Model layer
- Used MySQL transactions
- Used Soft deletes
- Versioning for static files

## How to setup
- Clone git repository
- Install Composer
- Install php 7.0.9+
- Install MySQL 5.6+
- Run `composer install`
- Duplicate .env.example and rename it to .env
- Update `APP_URL` of .env file
- Create blank database and execute
```
php artisan migrate
php artisan db:seed
```